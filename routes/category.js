var express = require('express')
var cls = require('indonesian-news-category-classifier')
var router = express.Router()

router.post('/', function(req, res) {
  var text = req.body.text
  var result = cls.predict(text)
  delete result.text
  res.send(result)
})

module.exports = router
